package application;

import java.util.Scanner;

import application.exception.InvalidParametersException;

public class Application {
    public static void main(String[] args) {
        Scanner terminal = new Scanner(System.in);
        System.out.println("Digite o primeiro parâmetro"); // NOSONAR
        int firstInt = terminal.nextInt();
        System.out.println("Digite o segundo parâmetro"); // NOSONAR
        int secondInt = terminal.nextInt();

        try {
            count(firstInt, secondInt);

        } catch (InvalidParametersException e) {
            System.out.println(e.getMessage()); // NOSONAR
        }
        terminal.close();
    }

    static void count(int firstInt, int secondInt) throws InvalidParametersException {
        if (firstInt >= secondInt) {
            throw new InvalidParametersException("O segundo parâmetro deve ser maior que o primeiro");
        }

        int contagem = secondInt - firstInt;
        for (int i = 1; i <= contagem; i++) {
            System.out.println("Imprimindo o número " + i); // NOSONAR
        }
    }
}
